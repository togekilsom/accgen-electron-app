var actualCode = `
async function sendSolution() {
    var url = new URL(document.referrer);
    var output = {
        token: CaptchaText(),
        gid: $J('#captchagid').val()
    }
    window.parent.postMessage(JSON.stringify(output), url.hostname.endsWith("cathook.club") ? url.origin : "https://accgen.cathook.club"); //get recaptcha token and send to accgen
}

var sendSolutionInterval = setInterval(function() { if (CaptchaText()) { sendSolution(); clearInterval(sendSolutionInterval); }}, 500);
`

function inIframe() {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

if (inIframe()) {
    window.onload = function () {
        //element to render recaptcha in
        document.getElementsByTagName("html")[0].innerHTML = `
        <input type="hidden" id="captchagid" name="captchagid" value="" />
        <div id="captcha_entry" style="display: none"><div id="captcha_entry_recaptcha"></div></div>`;
        var e = document.createElement("script");
        e.textContent = actualCode;
        (document.head || document.documentElement).appendChild(e);
    }
}
